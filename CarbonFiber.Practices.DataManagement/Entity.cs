﻿namespace CarbonFiber.Practices.DataManagement
{
    /// <summary>
    /// Represents the abstract data entity
    /// </summary>
    /// <typeparam name="TPrimaryKey">Specifies the type of the primary key</typeparam>
    public abstract class EntityBase<TPrimaryKey> where TPrimaryKey : struct
    {
        /// <summary>
        /// Gets or sets the entity primary key
        /// </summary>
        public virtual TPrimaryKey Id { get; set; }

        /// <summary>
        /// Initializes an instance of <see cref="EntityBase{TPrimaryKey}"/> class
        /// </summary>
        public EntityBase()
        {
        }
    }

    /// <summary>
    /// Represents data entity which is virtually deletable
    /// </summary>
    /// <typeparam name="TPrimaryKey">Specifies the type of the primary key</typeparam>
    public class DeletableEntity<TPrimaryKey> : EntityBase<TPrimaryKey>, IDeletable where TPrimaryKey : struct
    {
        /// <summary>
        /// Gets or sets a value indicates whether the entity is marked as deleted
        /// </summary>
        public virtual bool IsDeleted { set; get; }

        /// <summary>
        /// Initializes an instance of <see cref="DeletableEntity{TPrimaryKey}"/> class
        /// </summary>
        public DeletableEntity()
        {
        }
    }
}
