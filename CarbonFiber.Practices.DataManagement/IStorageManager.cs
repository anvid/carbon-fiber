﻿using System;
using System.Collections.Generic;

namespace CarbonFiber.Practices.DataManagement
{
    public interface IStorageManager : IDisposable
    {
        IStorageManager Add<T>(T item) where T : class, new();

        IStorageManager Delete<T>(T item, bool deleteVirtually) where T : class, IDeletable, new();

        IStorageManager Delete<T>(T item) where T : class, new();

        IStorageManager Update<T>(T item) where T : class, new();

        T Get<T>(object key) where T : class, new();

        IEnumerable<T> Get<T>() where T : class, new();

        bool SaveChanges();
    }
}