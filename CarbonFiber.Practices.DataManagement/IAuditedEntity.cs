﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarbonFiber.Practices.DataManagement
{

    /// <summary>
    /// Implements the properties of a fully audited entity
    /// </summary>
    /// <typeparam name="TUserPrimaryKey">Specifies the type of the primary key</typeparam>
    public interface IAuditedEntity<TUserPrimaryKey> : IDeletable where TUserPrimaryKey : struct
    {
        /// <summary>
        /// Gets or sets the Id of the user who created this entity
        /// </summary>
        TUserPrimaryKey CreatedUserId { get; set; }

        /// <summary>
        /// Gets or sets the date which this entity got created
        /// </summary>
        DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the Id of the user who modified this entity
        /// </summary>
        TUserPrimaryKey? ModifiedUserId { get; set; }

        /// <summary>
        /// Gets or sets the last modified date of the entity
        /// </summary>
        DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Gets or sets the Id of the user who modified this entity
        /// </summary>
        TUserPrimaryKey DeletedUserId { get; set; }

        /// <summary>
        /// Gets or sets the Id of the user who deleted (virtually) this entity
        /// </summary>
        DateTime DeletedOn { get; set; }
    }
}
