﻿using System.Collections.Generic;

namespace CarbonFiber.Practices.DataManagement
{
    public interface IQueryResult<T> where T : class, new()
    {
        IEnumerable<T> Data { get; set; }
        int TotalCount { get; set; }
        int FilteredCount { get; set; }
    }
}