﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarbonFiber.Practices.DataManagement
{
    public class ValidationResult
    {
        public string Field { get; set; }

        public string Message { get; set; }

        public ValidationResult(string message)
        {
            Message = message;
        }

        public ValidationResult(string field, string message)
        {
            this.Field = field;
            this.Message = message;
        }
    }
}
