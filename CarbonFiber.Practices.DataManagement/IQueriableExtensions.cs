﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarbonFiber.Practices.DataManagement
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> WhereIn<T, Tkey>(this IEnumerable<T> collection, Func<T, Tkey> key, params Tkey[] values)
        {
            return (from c in collection
                    where values.Contains(key(c))
                    select c).ToList();
        }

        public static IEnumerable<T> WhereIn<T, Tkey>(this IEnumerable<T> collection, Func<T, Tkey> key, IEnumerable<Tkey> values)
        {
            return (from c in collection
                    where values.Contains(key(c))
                    select c).ToList();
        }

        public static async Task<IEnumerable<T>> WhereInAsync<T, Tkey>(this DbSet<T> collection, Func<T, Tkey> key, params Tkey[] values)
            where T : class, new()
        {
            return await (from c in collection
                          where values.Contains(key(c))
                          select c).ToListAsync();
        }

        public static async Task<IEnumerable<T>> WhereInAsync<T, Tkey>(this DbSet<T> collection, Func<T, Tkey> key, IEnumerable<Tkey> values)
            where T : class, new()
        {
            return await (from c in collection
                          where values.Contains(key(c))
                          select c).ToListAsync();
        }
    }
}
