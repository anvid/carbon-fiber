﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarbonFiber.Practices.DataManagement
{
    public interface IValidatable
    {
        IEnumerable<ValidationResult> Validate();
    }

    public interface IValidatableAsync
    {
        Task<IEnumerable<ValidationResult>> ValidateAsync();
    }
}
