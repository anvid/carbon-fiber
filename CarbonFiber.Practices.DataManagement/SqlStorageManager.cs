﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CarbonFiber.Practices.DataManagement
{
    public class SqlStorageManager : IEntityStorageManager
    {
        #region Private Fields

        private readonly DbContext _dbContext;

        private bool disposed;

        #endregion Private Fields

        #region Public Constructors

        public SqlStorageManager(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion Public Constructors

        #region Public Methods

        public IStorageManager Add<T>(T item) where T : class, new()
        {
            _dbContext.Set<T>().Add(item);
            return this;
        }

        public IStorageManager Add<T>(IEnumerable<T> items) where T : class, new()
        {
            _dbContext.Set<T>().AddRange(items);
            return this;
        }

        public IStorageManager Attach<T>(T item) where T : class, new()
        {
            _dbContext.Set<T>().Attach(item);
            return this;
        }

        public IStorageManager AttachAll<T>(IEnumerable<T> items) where T : class, new()
        {
            foreach (var item in items)
            {
                _dbContext.Set<T>().Attach(item);
            }

            return this;
        }

        public DbSet<T> DataSet<T>() where T : class, new()
        {
            return _dbContext.Set<T>();
        }

        public IStorageManager Delete<T>(T item, bool deleteVirtually) where T : class, IDeletable, new()
        {
            if (!deleteVirtually)
            {
                _dbContext.Set<T>().Remove(item);
            }
            else
            {
                item.IsDeleted = true;
            }

            return this;
        }

        public IStorageManager Delete<T>(T item) where T : class, new()
        {
            _dbContext.Set<T>().Remove(item);
            return this;
        }

        public IStorageManager DeleteAll<T>(IEnumerable<T> items, bool deleteVirtually) where T : class, IDeletable, new()
        {
            if (!deleteVirtually)
            {
                foreach (var item in items)
                {
                    _dbContext.Set<T>().Remove(item);
                }
            }
            else
            {
                foreach (var item in items)
                {
                    item.IsDeleted = true;
                }
            }

            return this;
        }

        public IStorageManager DeleteAll<T>(IEnumerable<T> items) where T : class, new()
        {
            foreach (var item in items)
            {
                _dbContext.Set<T>().Remove(item);
            }

            return this;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual IQueryResult<TEntity> GetAsPages<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new()
        {
            IQueryable<TEntity> query = this.DataSet<TEntity>();

            int totalCount = this.DataSet<TEntity>().Count();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            int count = query.Count();

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (offset != null)
            {
                query = query.Skip(offset.Value);

            }

            if (take != null)
            {
                if (take > 0)
                {
                    query = query.Take(take.Value);
                }
            }

            return new QueryResult<TEntity>(query.ToList(), totalCount, count);
        }

        public IEnumerable<T> Get<T>() where T : class, new()
        {
            return this._dbContext.Set<T>().ToList();
        }

        public virtual TEntity Get<TEntity>(object id) where TEntity : class, new()
        {
            return this.DataSet<TEntity>().Find(id);
        }

        public bool SaveChanges()
        {
            return _dbContext.SaveChanges() > 0;
        }

        public IStorageManager Update<T>(T item) where T : class, new()
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            return this;
        }

        public IStorageManager UpdateAll<T>(IEnumerable<T> items) where T : class, new()
        {
            foreach (var item in items)
            {
                _dbContext.Entry(item).State = EntityState.Modified;
            }

            return this;
        }

        #endregion Public Methods

        #region Protected Methods

        public async Task<IQueryResult<TEntity>> GetAsPagesAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "",
            int? offset = null, int? take = null) where TEntity : class, new()
        {
            IQueryable<TEntity> query = this.DataSet<TEntity>();

            int totalCount = await this.DataSet<TEntity>().CountAsync();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            int count = await query.CountAsync();

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (offset != null)
            {
                query = query.Skip(offset.Value);

            }

            if (take != null)
            {
                if (take > 0)
                {
                    query = query.Take(take.Value);
                }
            }

            return new QueryResult<TEntity>(await query.ToListAsync(), totalCount, count);
        }

        public async Task<IEnumerable<T>> GetAsync<T>() where T : class, new()
        {
            return await this._dbContext.Set<T>().ToListAsync();
        }

        public async virtual Task<T> GetAsync<T>(object key) where T : class, new()
        {
            return await Task.Run(() =>
            {
                return this.DataSet<T>().Find(key);
            });
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._dbContext.Dispose();
                }
            }

            this.disposed = true;
        }

        public IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "",
            int? offset = null, int? take = null) where TEntity : class, new()
        {
            IQueryable<TEntity> query = this.DataSet<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (offset != null)
            {
                query = query.Skip(offset.Value);

            }

            if (take != null)
            {
                if (take > 0)
                {
                    query = query.Take(take.Value);
                }
            }

            return query.ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAsync<TEntity>(Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new()
        {
            IQueryable<TEntity> query = this.DataSet<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (offset != null)
            {
                query = query.Skip(offset.Value);

            }

            if (take != null)
            {
                if (take > 0)
                {
                    query = query.Take(take.Value);
                }
            }

            return await query.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> In<TEntity, TKey>(Func<TEntity, TKey> field, params TKey[] values) where TEntity : class, new()

        {
            if (field == null)
            {
                throw new ArgumentNullException(nameof(field));
            }

            if (!values.Any())
            {
                throw new ArgumentException("Invalid argument", nameof(values));
            }

            return await DataSet<TEntity>().WhereInAsync(field, values);
        }

        #endregion Protected Methods
    }
}