﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CarbonFiber.Practices.DataManagement
{
    /// <summary>
    /// Implements the data entity storage management functionalities
    /// </summary>
    public interface IEntityStorageManager : IStorageManager
    {
        #region Public Methods

        /// <summary>
        /// Adds the specified data entities to the entity set
        /// </summary>
        /// <typeparam name="T">Type of the data entity</typeparam>
        /// <param name="items">Specifies the data entities to be added</param>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type</returns>
        IStorageManager Add<T>(IEnumerable<T> items) where T : class, new();

        /// <summary>
        /// Attaches the data entity to the store
        /// </summary>
        /// <typeparam name="T">Specifies the data entities to be added</typeparam>
        /// <param name="item">Specifies the data entity to be attached</param>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type</returns>
        IStorageManager Attach<T>(T item) where T : class, new();

        /// <summary>
        /// Attaches the data entities to the entity set
        /// </summary>
        /// <typeparam name="T">Specifies the type of the entities</typeparam>
        /// <param name="items">Specifies the data entities to be attached</param>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type<</returns>
        IStorageManager AttachAll<T>(IEnumerable<T> items) where T : class, new();

        /// <summary>
        /// Retrieves the associated dataset of the data entity
        /// </summary>
        /// <typeparam name="T">Specifies the data type of the entity</typeparam>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type</returns>
        DbSet<T> DataSet<T>() where T : class, new();

        /// <summary>
        /// Deletes virtually the data entities from the entity set
        /// </summary>
        /// <typeparam name="T">Specifies the type of the data entity</typeparam>
        /// <param name="items">Specifies the data entities to be deleted</param>
        /// <param name="deleteVirtually">Marks the data entities as virtually deleted</param>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type</returns>
        IStorageManager DeleteAll<T>(IEnumerable<T> items, bool deleteVirtually) where T : class, IDeletable, new();

        /// <summary>
        /// Deletes permanently the data entities from the entity set
        /// </summary>
        /// <typeparam name="T">Specifies the type of the data entity</typeparam>
        /// <param name="items">Specifies the data entities to be deleted</param>
        /// <returns>Returns the same instance of <see cref="IStorageManager"/> type</returns>
        IStorageManager DeleteAll<T>(IEnumerable<T> items) where T : class, new();

        /// <summary>
        /// Gets the data entities as paged list
        /// </summary>
        /// <typeparam name="TEntity">Specifies the type of the entity</typeparam>
        /// <param name="filter">Specifies the criteria to fetch the data entity list</param>
        /// <param name="orderBy">Specifies the column to be sorted by for the resulting list</param>
        /// <param name="includeProperties">Specifies the associated navigational properties to be loaded on fetching data entities</param>
        /// <param name="offset">Specifies the number data entities to be skipped on fetching the list</param>
        /// <param name="take">Specifies the number of data entities to be included in the resulting collection</param>
        /// <returns>Returns an instance of <see cref="IQueryResult<TEntity>"/> entity</returns>
        IQueryResult<TEntity> GetAsPages<TEntity>(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        /// <summary>
        /// Gets the entities based on the filter criteria
        /// </summary>
        /// <typeparam name="TEntity">Specifies the type of the entity</typeparam>
        /// <param name="filter">Specifies the criteria to be used on fetching the data entities</param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <param name="offset"></param>
        /// <param name="take"></param>
        /// <returns>Returns a collection of the data entity</returns>
        IEnumerable<TEntity> Get<TEntity>(
    Expression<Func<TEntity, bool>> filter,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        /// <summary>
        /// Gets the entities based on the filter criteria
        /// </summary>
        /// <typeparam name="TEntity">Specifies the type of the entity</typeparam>
        /// <param name="filter">Specifies the criteria to be used on fetching the data entities</param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <param name="offset"></param>
        /// <param name="take"></param>
        /// <returns>Returns a collection of the data entity</returns>
        Task<IEnumerable<TEntity>> GetAsync<TEntity>(
Expression<Func<TEntity, bool>> filter,
Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        /// <summary>
        /// Gets the data entities as paged list
        /// </summary>
        /// <typeparam name="TEntity">Specifies the type of the entity</typeparam>
        /// <param name="filter">Specifies the criteria to fetch the data entity list</param>
        /// <param name="orderBy">Specifies the column to be sorted by for the resulting list</param>
        /// <param name="includeProperties">Specifies the associated navigational properties to be loaded on fetching data entities</param>
        /// <param name="offset">Specifies the number data entities to be skipped on fetching the list</param>
        /// <param name="take">Specifies the number of data entities to be included in the resulting collection</param>
        /// <returns>Returns an instance of <see cref="IQueryResult<TEntity>"/> entity</returns>
        Task<IQueryResult<TEntity>> GetAsPagesAsync<TEntity>(
    Expression<Func<TEntity, bool>> filter,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        /// <summary>
        /// Retrieves the data entities having the values for the property, specified the in the list
        /// </summary>
        /// <typeparam name="TEntity">Specifies the entity data type</typeparam>
        /// <typeparam name="TField">Specifies the type of the field</typeparam>
        /// <param name="field">Specifies the name of the field</param>
        /// <param name="values">Specifies the values to be matched with the property value</param>
        /// <returns>Returns an instance of <see cref="Task<IEnumerable<TEntity>>"/> class</returns>
        Task<IEnumerable<TEntity>> In<TEntity, TField>(Func<TEntity, TField> field, params TField[] values) where TEntity : class, new();

        /// <summary>
        /// Fetches all data entities of the specified entity from the store
        /// </summary>
        /// <typeparam name="T">Specifies the type of the data entity</typeparam>
        /// <returns>Returns the collection of the data entities</returns>
        Task<IEnumerable<T>> GetAsync<T>() where T : class, new();

        /// <summary>
        /// Retrieves a data entity with the specified key
        /// </summary>
        /// <typeparam name="T">Specifies the type of the data entity</typeparam>
        /// <param name="key">Specifies the key to identify the data entity</param>
        /// <returns>Returns an instance of the data entity</returns>
        Task<T> GetAsync<T>(object key) where T : class, new();

        /// <summary>
        /// Commits the data entity map at entity store
        /// </summary>
        /// <returns>Returns number of records affected in the entity store</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Updates the specified entities at entity set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        IStorageManager UpdateAll<T>(IEnumerable<T> items) where T : class, new();

        #endregion Public Methods
    }
}