﻿namespace CarbonFiber.Practices.DataManagement
{
    /// <summary>
    /// Implements the property to make an data entity as virtually deletable
    /// </summary>
    public interface IDeletable
    {
        /// <summary>
        /// Gets or sets a value indicating whether the entity is marked as deleted virtually
        /// </summary>
        bool IsDeleted { get; set; }
    }
}
