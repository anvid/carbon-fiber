﻿using System.Collections.Generic;

namespace CarbonFiber.Practices.DataManagement
{
    public class QueryResult<T> : IQueryResult<T> where T : class, new()
    {
        public IEnumerable<T> Data { get; set; }

        public int TotalCount { get; set; }

        public int FilteredCount { get; set; }

        public QueryResult()
        {

        }

        public QueryResult(IEnumerable<T> data, int totalCount, int fiteredCount)
        {
            this.Data = data;
            this.TotalCount = totalCount;
            this.FilteredCount = fiteredCount;
        }
    }
}
